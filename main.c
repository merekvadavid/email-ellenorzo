#include <stdio.h>

int check_validity(char* address) {

  if(address[0] == '@')
  	return -2;

	int at_count = 0;
  int dot_count = 0;
  char previous_char = ' ';
  int invalid_sequence = 0;

  int i;
  for(i = 0; address[i] != '\0'; i++) {
  	if(address[i] == '@')
    	at_count++;

    if(address[i] == '.')
    	dot_count++;

    if(previous_char == '@' && (address[i] == '@' || address[i] == '.'))
      invalid_sequence = -1;

    if(previous_char == '.' && address[i] == '@')
      invalid_sequence = -1;

    previous_char = address[i];
  }

  if(at_count != 1)
  	return -3;

  if(dot_count < 1)
  	return -4;

  if(invalid_sequence != 0)
  	return -5;

  if(i < 5)
    return -1;

  return 0;
}



int main(int argc, char* argv[]) {

  if(argc < 2)
  	return -10;
	printf(argv[1]);
  int valid = check_validity(argv[1]);

  return valid;
}


